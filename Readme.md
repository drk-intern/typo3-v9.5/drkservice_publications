# Publikationen

Diese Erweiterung stellt Publikationen dar

![image.png](Documentation%2Fimage.png)

## Installation

1. `composer req drkservice/drk-publications`
2. TYPOScript im Template der betroffenen Seiten einfügen
3. PageTS im Template der betroffenen Seiten einfügen

## Nutzung

1. News anlegen
2. News plugin anlegen
   1. Storagefolder wählen
   2. Template auf Publikation stellen

# Hinweise zur Nutzung

