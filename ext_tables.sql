#
# Table structure for table 'tx_news_domain_model_news'
#
CREATE TABLE tx_news_domain_model_news (
	publicationFormat varchar(255) DEFAULT '' NOT NULL,
	publicationPagesCount varchar(255) DEFAULT '' NOT NULL,
	publicationLanguage varchar(255) DEFAULT '' NOT NULL,
	publicationItemNumber varchar(255) DEFAULT '' NOT NULL,
	publicationIssn varchar(255) DEFAULT '' NOT NULL,
	publicationIsbn varchar(255) DEFAULT '' NOT NULL,
	publicationEan varchar(255) DEFAULT '' NOT NULL,
	publicationOrdernumber varchar(255) DEFAULT '' NOT NULL,
	publicationPublisher varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	publicationNotes text,
	publicationOrdertext text,
);
