<?php

namespace Drkservice\Publications\Domain\Model;

class NewsPublication extends \GeorgRinger\News\Domain\Model\News
{
    /**
     * Magic Number to identify the RECORD_TYPE_ID in the tx_news table
     */
    public const RECORD_TYPE_ID = 1700227046;

    /**
     * Magic Number to identify the PUBLICATION_LIST_VIEW in the flexform template field
     */
    public const PUBLICATION_LIST_VIEW_ID = 1700212379;

    public string $publicationFormat = '';
    public string $publicationPagesCount = '';
    public string $publicationLanguage = '';
    public string $publicationPublisher = '';
    public string $publicationItemNumber = '';
    public string $publicationIssn = '';
    public string $publicationIsbn = '';
    public string $publicationEan = '';
    public string $publicationOrdernumber = '';
    public string $publicationNotes = '';
    public string $publicationOrdertext = '';

    public function getPublicationFormat(): string
    {
        return $this->publicationFormat;
    }

    public function setPublicationFormat(string $publicationFormat): void
    {
        $this->publicationFormat = $publicationFormat;
    }

    public function getPublicationPagesCount(): string
    {
        return $this->publicationPagesCount;
    }

    public function setPublicationPagesCount(string $publicationPagesCount): void
    {
        $this->publicationPagesCount = $publicationPagesCount;
    }

    public function getPublicationLanguage(): string
    {
        return $this->publicationLanguage;
    }

    public function setPublicationLanguage(string $publicationLanguage): void
    {
        $this->publicationLanguage = $publicationLanguage;
    }

    public function getPublicationPublisher(): string
    {
        return $this->publicationPublisher;
    }

    public function setPublicationPublisher(string $publicationPublisher): void
    {
        $this->publicationPublisher = $publicationPublisher;
    }

    public function getPublicationItemNumber(): string
    {
        return $this->publicationItemNumber;
    }

    public function setPublicationItemNumber(string $publicationItemNumber): void
    {
        $this->publicationItemNumber = $publicationItemNumber;
    }

    public function getPublicationIssn(): string
    {
        return $this->publicationIssn;
    }

    public function setPublicationIssn(string $publicationIssn): void
    {
        $this->publicationIssn = $publicationIssn;
    }

    public function getPublicationIsbn(): string
    {
        return $this->publicationIsbn;
    }

    public function setPublicationIsbn(string $publicationIsbn): void
    {
        $this->publicationIsbn = $publicationIsbn;
    }

    public function getPublicationEan(): string
    {
        return $this->publicationEan;
    }

    public function setPublicationEan(string $publicationEan): void
    {
        $this->publicationEan = $publicationEan;
    }

    public function getPublicationOrdernumber(): string
    {
        return $this->publicationOrdernumber;
    }

    public function setPublicationOrdernumber(string $publicationOrdernumber): void
    {
        $this->publicationOrdernumber = $publicationOrdernumber;
    }

    public function getPublicationNotes(): string
    {
        return $this->publicationNotes;
    }

    public function setPublicationNotes(string $publicationNotes): void
    {
        $this->publicationNotes = $publicationNotes;
    }

    public function getPublicationOrdertext(): string
    {
        return $this->publicationOrdertext;
    }

    public function setPublicationOrdertext(string $publicationOrdertext): void
    {
        $this->publicationOrdertext = $publicationOrdertext;
    }
}
