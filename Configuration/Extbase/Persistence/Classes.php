<?php
return [
    \GeorgRinger\News\Domain\Model\News::class => [
        'subclasses' => [
            \Drkservice\Publications\Domain\Model\NewsPublication::RECORD_TYPE_ID => \Drkservice\Publications\Domain\Model\NewsPublication::class,
        ]
    ],
    \Drkservice\Publications\Domain\Model\NewsPublication::class => [
        'tableName' => 'tx_news_domain_model_news',
        'recordType' => \Drkservice\Publications\Domain\Model\NewsPublication::RECORD_TYPE_ID,
    ]
];
