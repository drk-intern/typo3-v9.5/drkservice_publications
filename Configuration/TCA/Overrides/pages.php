<?php

defined('TYPO3') or die();
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

(function() {
    ExtensionManagementUtility::registerPageTSConfigFile(
        'drkservice_publications',
        'Configuration/PageTS/PageTSconfig.typoscript',
        'DRK Publications'
    );
})();

