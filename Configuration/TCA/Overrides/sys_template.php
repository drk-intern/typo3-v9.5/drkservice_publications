<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

(function() {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        'drkservice_publications',
        'Configuration/TypoScript',
        'DRK Publications'
    );
})();
