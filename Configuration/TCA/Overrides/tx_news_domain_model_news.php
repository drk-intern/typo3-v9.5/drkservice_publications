<?php

$boot = static function (int $redordTypeId, string $label): void {
    $ll = 'LLL:EXT:drkservice_publications/Resources/Private/Language/locallang_db.xlf:';

    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['type']['config']['items'][(string)$redordTypeId] =
        [
            'label' => $label,
            'value' => $redordTypeId
        ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationFormat'] = [
        'exclude' => false,
        'label' => $ll . 'publicationFormat',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
            'placeholder' => 'A4, A5, ...',
        ],
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationPagesCount'] = [
        'exclude' => false,
        'label' => $ll . 'publicationPagesCount',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationLanguage'] = [
        'exclude' => false,
        'label' => $ll . 'publicationLanguage',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationItemNumber'] = [
        'exclude' => false,
        'label' => $ll . 'publicationItemNumber',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationIssn'] = [
        'exclude' => false,
        'label' => $ll . 'publicationIssn',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationEan'] = [
        'exclude' => false,
        'label' => $ll . 'publicationEan',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationOrdernumber'] = [
        'exclude' => false,
        'label' => $ll . 'publicationOrdernumber',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationPublisher'] = [
        'exclude' => false,
        'label' => $ll . 'publicationPublisher',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];

    /** Text */
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationNotes'] = [
        'exclude' => false,
        'label' => $ll . 'publicationNotes',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];

    /** Text */
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['publicationOrdertext'] = [
        'exclude' => false,
        'label' => $ll . 'publicationOrdertext',
        'config' => [
            'type' => 'input',
            'size' => 60,
            'max' => 255,
        ],
    ];

    /** Publication palette */
    $GLOBALS['TCA']['tx_news_domain_model_news']['palettes']['drkservice_publication_palette_1'] = [
        'label' => $ll . 'palette_publications',
        'showitem' => 'publicationFormat, publicationPagesCount, publicationLanguage, publicationPublisher',
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['palettes']['drkservice_publication_palette_2'] = [
        'label' => $ll . 'palette_publications',
        'showitem' => 'publicationItemNumber, publicationIssn, publicationEan, publicationOrdernumber',
    ];
    $GLOBALS['TCA']['tx_news_domain_model_news']['palettes']['drkservice_publication_palette_3'] = [
        'label' => $ll . 'palette_publications',
        'showitem' => 'publicationNotes, publicationOrdertext',
    ];

    /** Type for publication */
    $GLOBALS['TCA']['tx_news_domain_model_news']['types'][(string)$redordTypeId] = [
        'showitem' => implode(
            ',',
            [
                '--palette--;;paletteCore',
                'title',
                '--palette--;;paletteSlug',
                '--palette--;;paletteDate',
                'bodytext',
                '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.media',
                    'fal_media',
                    'fal_related_files',
                '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories',
                    'categories',
                '--div--;LLL:EXT:news/Resources/Private/Language/locallang_db.xlf:tx_news_domain_model_news.tabs.relations',
                    'related',
                    'related_from',
                    'related_links',
                    'tags',
                '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata',
                    '--palette--;' . $ll . 'palette_publications_1;drkservice_publication_palette_1',
                    '--palette--;' . $ll . 'palette_publications_2;drkservice_publication_palette_2',
                    '--palette--;' . $ll . 'palette_publications_3;drkservice_publication_palette_3',
                    '--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;paletteAuthor',
                    '--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags',
                    '--palette--;' . $ll . 'tx_news_domain_model_news.palettes.alternativeTitles;alternativeTitles',
                '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language',
                    '--palette--;;paletteLanguage',
                '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access',
                    '--palette--;;paletteHidden',
                    '--palette--;;paletteAccess',
                '--div--;' . $ll . 'notes',
                    'notes',
                '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended',
            ]
        )
    ];
};

$boot(
    \Drkservice\Publications\Domain\Model\NewsPublication::RECORD_TYPE_ID,
    'Publikation'
);
unset($boot);
